// passing 2 D array to a function in c

#include<stdio.h>

void fun(int rows,int cols,int arr[rows][cols]){
	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){

			arr[i][j] = 10 + arr[i][j];
		}
	}
	
}

void main() {
	int cols,rows;

	printf("Enter array rows: ");
	scanf("%d",&rows);

	printf("Enter array cols: ");
	scanf("%d", &cols);
	
	int arr[rows][cols];
	
	printf("Enter array Elements: ");
	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
			scanf("%d",&arr[i][j]);
		}
	}
	
	fun(rows,cols,arr);
	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
			printf("%d\n",arr[i][j]);
		}
	}
}

