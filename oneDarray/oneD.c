// passing array to a function in c

#include<stdio.h>

void fun(int arr[], int size){
	for(int i = 0; i < size; i++){
		arr[i] = 10 + arr[i];
	}
}

void main() {
	int size;
	printf("Enter array size: ");
	scanf("%d", &size);
	int arr[size];
	printf("Enter array Elements: ");
	for(int i = 0; i < size; i++){
		scanf("%d",&arr[i]);
	}
	fun(arr,size);
	for(int i = 0; i < size; i++){
		printf("%d\n",arr[i]);
	}

}

