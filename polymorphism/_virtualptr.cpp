
#include<iostream>

class parent {
	public:
		// void *_vptr;   // By default it comes here don need to write it 
		virtual void getdata() {
			std::cout << "parent getdata" << std::endl;
		}
		virtual void printdata() {
			std::cout << "parent printdata" << std::endl;
		}
};

class child1 : public parent{
	public: 
		// _vptr;
		void getdata() {
			std::cout << "child1 getdata" << std::endl;
		}
};

class child2 : public parent {
	public : 
		void getdata() {
			std::cout << "child2 getdata " << std::endl;
		}
};

int main() {
	parent * obj = new child2();
	obj->getdata();
}



