// pure virtual function

#include<iostream>

class parent{
	public:
		virtual void marry() = 0;
		virtual void property() {
			std::cout << "flat,car,gold" << std::endl;
		}
};

void parent::marry() {
	std::cout << "kirti" << std::endl;
}

class child:public parent{
	public:
		void marry(){
			std::cout << "Disha" << std::endl;
		}
};

int main() {
	//parent obj;
	parent obj = new child();
	obj -> marry();
	obj -> property();
}

