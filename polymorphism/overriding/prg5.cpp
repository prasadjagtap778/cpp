

#include<iostream>

class parent{
	public:
		virtual void getdata(int x) const {
			std::cout << "getdata - parent" << std::endl;
		}
		
};

class child : public parent{
	public:
		void getdata(int x) const override {
			std::cout << "child getdata" << std::endl;
		}
};

int main(){
	parent *obj = new child();
	obj->getdata(10);
	return 0;
}


