#include<iostream>

class parent{
	public:
		parent(){
			std::cout<< "parent constructor" << std::endl;
		}
		virtual void getdata(){
			std::cout << "parent getdata" << std::endl;
		}
};

class child : public parent{
	public:
		child(){
			std::cout << "child constructor" << std::endl;
		}	
		void getdata(){  //implicit get calls to virtual 
			//parent::getdata();
			std::cout << "child getdata" << std::endl;
		}
};

int main(){
	parent *obj = new child();
	obj -> getdata();
	return 0;
}


