// NOTE:- class is same as struct in cpp but major difference is that we cannot
// access directly the varialbles in class into main function the error occures
// private variable cannot access into main. BUT in struct we can access the private variable very easily
// their is no error in struct
// ******************to access we have to make a block public: ****************

#include<iostream>

class player{
	 
	int jerNo = 18;
	char name[20] = "virat kohli";
	
	public:
	void disp(){
		std::cout<< jerNo <<std::endl;
		std::cout<< name <<std::endl;
	}
};

int main(){
	player obj;
//	std::cout<< obj.jerNo <<std::endl;
//	std::cout<< obj.name <<std::endl;
	obj.disp();

	return 0;
}

