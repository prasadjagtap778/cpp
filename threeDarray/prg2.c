// passing 3 D array to a function in c

#include<stdio.h>

void fun(int planes,int rows,int cols,int arr[planes][rows][cols]){
	for(int i = 0; i < planes; i++){
		for(int j = 0; j < rows; j++){
			for(int k = 0; k < cols; k++){
				arr[i][j][k] = 10 + arr[i][j][k];
			}
		}
	}

	for(int i = 0; i < planes; i++){
		for(int j = 0; j < rows; j++){
			for(int k = 0; k < cols; k++){
				printf("%d ",*(*(*(arr+i)+j)+k));
			}
			printf("\n");
		}
		printf("\n");
	}
	
}

void main() {
	int planes,cols,rows;
	
	printf("Enter array palnes: ");
	scanf("%d",&planes);


	printf("Enter array rows: ");
	scanf("%d",&rows);

	printf("Enter array cols: ");
	scanf("%d", &cols);
	
	int arr[planes][rows][cols];
	
	printf("Enter array Elements: ");
	for(int i = 0; i < planes; i++){
		for(int j = 0; j < rows; j++){
			for(int k = 0; k < cols; k++){
				scanf("%d",&arr[i][j][k]);
			}
		}
	}
	
	fun(planes,rows,cols,arr);
	/*
	for(int i = 0; i < planes; i++){
		for(int j = 0; j < rows; j++){
			for(int k = 0; k < cols; k++){
				printf("%d ",arr[i][j][k]);
			}
			printf("\n");
		}
		printf("\n");
	}
	*/
}


