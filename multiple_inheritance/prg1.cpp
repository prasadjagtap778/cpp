#include<iostream>
class parent1{
	public:
		parent1() {
			std::cout << "constructor parent1" << std::endl;
		}
};

class parent2 {
	public:
		parent2() {
			std::cout << "constructor parent2" << std::endl;
		}
};

class child:public parent1,public parent2{
	public:
		child(){
			std::cout << "constructor child" << std::endl;
		}
};

int main(){
	child obj;
	return 0;
}

