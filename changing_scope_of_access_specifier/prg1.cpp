#include<iostream>

class parent{
	int x = 10;
	
	protected:
	int y = 20;

	public:
	int z = 30;
	void getData(){
		std::cout << "In getData" << std::endl;
	}
};

class child: public parent{
	using parent::getData;
	public:
	using parent::y;
};

int main(){
	child obj;
	std::cout << obj.y << obj.z << std::endl;
	obj.getData();
	return 0;
}


