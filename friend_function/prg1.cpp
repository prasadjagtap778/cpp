#include<iostream>

class parent{
	int x = 10;
	public:

	parent(){
		std::cout << "parent = " << x << std::endl;
	}
	friend std::ostream& operator << (std::ostream& out,const parent& obj){
		out << "In parent" << std::endl;
		out << obj.x;
		return out;
	}
	parent(parent& obj) {
		std::cout << "copy" << std::endl;
	}
};

class child:public parent{
	int x = 20;
	public:

	child() {
		std::cout << "child" << std::endl;
	}
	friend std::ostream& operator << (std::ostream& out,const child& obj){
		out << "In child" << std::endl;
		out << obj.x;
		return out;
	}
};

int main(){
	child obj1;
	std::cout << (const parent) obj1 << std::endl;  // copy
	//std::cout << (const parent&) obj1 << std::endl;  // no call to copy
	child obj2;
	std::cout<< obj2 << std::endl;
	//operator<< (ostream,child&)
	return 0;
}

