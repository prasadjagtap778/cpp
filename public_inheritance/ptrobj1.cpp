//to print parent getdata member function while child object is created
// 1) parent::getData();
// 2) (parent(obj)).getData();
// 3) obj.parent::getData()
// 4) (static_cast<parent&>(obj)).getData();


#include<iostream>

class parent{
	int x = 10;
	public:
	parent(){
		std::cout << "Constructor Parent" << std::endl;
	}
	void getData(){
		std::cout << "parent x = " << x << std::endl;
	}
};

class child:public parent{
	int x = 20;
	public:
	child(){
		std::cout << "Constructor Child" << std::endl;
	}
	void getData(){
		// parent::getData();   // 1)
		std::cout << "Child x = " << x << std::endl;
	}
};

int main() {
	
	child obj;
	//(parent(obj)).getData();    // 2) 

	//obj.parent::getData();	// 3)
	//(static_cast<parent&>(obj)).getData();     // 4)
	obj.getData();

	return 0;
}

