#include<iostream>

class parent{
	int x = 10;
	
	protected:
	int y = 20;

	public:
	int z = 30;

	parent(){
		std::cout<< "parent Constructor" << std::endl;
	}
};

class child:private parent{
	public:
		child(){
			std::cout<< "Child Constructor" << std::endl;
		}
};

int main(){
	child obj;
	std::cout << obj.x << obj.y << obj.z << std::endl;
	//std::cout<< sizeof(obj) << std::endl;
	return 0;
}

