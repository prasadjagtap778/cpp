#include<iostream>

class parent{
	int x = 10;
	public:
	parent(){
		std::cout << "constructor parent" << std::endl;
	}
	void getdata() {
		std::cout << x << std::endl;
	}
};

class child:public parent{
	int y = 20;
	public:
	child(){
		std::cout << "Constructor Child" << std::endl;
	}
	void printdata(){
		std::cout << y << std::endl;
	}
};

int main(){
	child obj;
	obj.getdata();
	obj.printdata();
	
	
}

