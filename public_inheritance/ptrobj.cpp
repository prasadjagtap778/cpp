#include<iostream>

class parent{
	int x = 10;
	public:
	parent(){
		std::cout << "Constructor Parent" << std::endl;
	}
	void getData(){
		std::cout << "parent x = " << x << std::endl;
	}
};

class child:public parent{
	int x = 20;
	public:
	child(){
		std::cout << "Constructor Child" << std::endl;
	}
	void getData(){
		std::cout << "Child x = " << x << std::endl;
	}
};

int main() {
	parent *obj = new child();
	obj->getData();
	return 0;
}

