#include<iostream>

class parent{
	int x = 10;
	int y = 20;
	public:
		int a = 30;
		parent(){
			std::cout << "parent constructor" << std::endl;
		}
		~parent() {
			std::cout << "parent destructor" << std::endl;
		}

		void getdata() {
			std::cout << x << " " << y << " " << a << std::endl;
		}
};

class child: public parent{
	int z = 10;
	public:
		child(){
			std::cout << "child constructor " << std::endl;
		};
		~child(){
			std::cout << "child destructor" << std::endl;
		}

		void printData(){
			std::cout << a << " " << z << std::endl;
		}
};

int main(){
	child obj2;
	obj2.getdata();
	obj2.printData();
	return 0;
}
