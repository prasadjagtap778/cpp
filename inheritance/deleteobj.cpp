#include<iostream>

class parent{
	public:
		parent(){
			std::cout<<"constructure parent"<<std::endl;
		}
		~parent(){
			std::cout<<"Destructure parent"<<std::endl;
		}
};

class child:public parent{
	public:
		child(){
			std::cout<<"Constructure child"<< std::endl;
		}
		~child() {
			std:: cout << "destructure child" << std::endl;
		}

		friend void * operator new(size_t size){
			std::cout<<"new child"<<std::endl;
			void *ptr = malloc(size);
			return ptr;
		}

		void operator delete (void *ptr){
			std::cout<<"delete child" << std::endl;
			free(ptr);
		}
};

int main(){
	child *obj1 = new child();
	//operator new (sizeof(child))
	//obj1 = ptr;
	//child(obj1);
	//constructor
		//constructure parent
		//constructure child
	delete obj1;
	//destructure ==== Notification
		//destructor child
		//destructor parent
	//operator delete(obj1)
	return 0;
}











