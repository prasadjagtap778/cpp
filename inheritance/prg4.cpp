#include<iostream>

class parent{
	int x = 10;
	int y = 20;
	public: 
		parent(int x = 0, int y = 0){
			std::cout << "In parent Constructor" << std::endl;
			this -> x = x;
			this -> y = y;
		}
		
		void getdata(){
			std::cout << x << std::endl;
			std::cout << y << std::endl;
		}
};

class child:public parent{
	int z = 30;
	public:
		child(int x, int y, int z){
			std::cout << "child constructor" << std::endl;
		}

		void printdata(){
			std::cout << z << std::endl;
		}
};

int main() {
	child obj(40,50,60);
	obj.getdata();
	obj.printdata();
	return 0;
}


