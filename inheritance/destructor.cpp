#include<iostream>

class parent{
	public:
		parent(){
			std::cout<<"Constructor parent"<<std::endl;
		}
		~parent(){
			std::cout<<"Destructurte parent"<<std::endl;
		}
};

class child:public parent{
	public :
		child(){
			std::cout<<"constructure child"<<std::endl;
		}
		~child(){
			std::cout<<"Desturcture child"<<std::endl;
		}
};

int main(){
	child obj1;
	child *obj2 = new child();
	delete obj2;
	return 0;
}
